#!/usr/bin/env python3
import boto3
import botocore
import logging
import json
import os
import sys
from pprint import pformat
import dateutil.parser


loglevel = os.environ.get('LogLevel', logging.INFO)
logger = logging.getLogger()
logger.setLevel(loglevel)
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)


ec2_client = boto3.client('ec2')


def get_ami(ami_filters):
    filters = list()
    for key, value in ami_filters.items():
        filter_dict = dict()
        filter_dict['Name'] = key
        filter_dict['Values'] = value
        filters.append(filter_dict)
    target_ami = ec2_client.describe_images(
        Filters=filters
    )
    if target_ami and len(target_ami['Images']) == 1:
        return target_ami['Images'][0]['ImageId']
    elif target_ami and len(target_ami['Images']) > 1:
        newest_ami_id = target_ami['Images'][0]['ImageId']
        newest_ami_date = dateutil.parser.parse(target_ami['Images'][0]['CreationDate'])
        for ami in target_ami['Images']:
            if dateutil.parser.parse(ami['CreationDate']) > newest_ami_date:
                newest_ami_id = ami['ImageId']
                newest_ami_date = ami['CreationDate']
            else:
                continue
        return newest_ami_id
    else:
        raise Exception('No AMI found')


def lambda_handler(event, context):
    logger.info("Lambda received the event {}".format(pformat(event)))
    logger.info('Starting...')
    try:
        ami_id = get_ami(event.get('filters'))
    except Exception as e:
        logger.error("Something went wrong when getting the AMI:\n{}".format(e))
    else:
        logger.info("Done!")
        print(ami_id)
        return {
            'ami-id': ami_id,
        }


if __name__ == "__main__":
    event = json.loads(sys.argv[1])
    lambda_handler(event, context=None)
